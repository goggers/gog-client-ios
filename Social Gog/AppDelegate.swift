//
//  AppDelegate.swift
//  Social Gog
//
//  Created by Alexander Kozlovskij on 12.08.18.
//  Copyright © 2018 gog. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

	var window: UIWindow?
	var ctx: UnsafeMutablePointer<GogCtx>?;
	var delegate: GogDelegate?;

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		// ERROR: Could not cast value of type 'UINavigationController' (0x3b8fa5e0) to 'UISplitViewController' (0x3b8fadb0).
//		let splitViewController = window!.rootViewController as! UISplitViewController;
//		let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
//		if #available(iOS 8.0, *) {
//			navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
//		} else {
//			// Fallback on earlier versions
//		}
//		splitViewController.delegate = self
		
		//		var ctx:UnsafePointer<Ctx>? = Optional.none;
		//		self.ctx = gog_create_context({ (error) in
		self.ctx = gog_create_context_with_printing_delegate({ (error) in
			print("(in block) gog context error.code: \(errorCode(err: error))");
			print("(in block) gog context error: \(toString(err: error))");
			// print("(in block) gog context error.description: \(errorDescription(err: error))");
		});
		
		// Optional but really needed to recive new data - set delegate:
		// self.delegate = gog_create_empty_delegate();
		//		self.delegate = gog_create_printing_delegate();
		//		self.ctx?.pointee.delegate = self.delegate!;
		//		self.ctx?.pointee.delegate.
		
		
		
		// configure web-view:
//		self.browser.allowsLinkPreview = false;
//		self.browser.allowsBackForwardNavigationGestures = false;
//		self.browser.addObserver(self, forKeyPath: #keyPath(WKWebView.url), options: .new, context: nil);
		//		browser.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil);
		
		// initialize GoG API Client:
		if false {
			// If and when we have `client` deserialized from local db:
			//			gog_import_context(ctx, <#T##client: UnsafePointer<ExportGogClient>!##UnsafePointer<ExportGogClient>!#>, <#T##err: ErrBlock!##ErrBlock!##(UnsafePointer<GogError>?) -> Void#>);
			
			gog_init_hot(ctx, {
				print("(in block) gog hot init complete");
			}, { (error) in
				print("(in block) gog hot init error: \(toString(err: error!))");
			});
			
		} else {
			
			gog_init_cold(ctx, {
				print("(in block) gog cold init complete");
			}, { (error) in
				print("(in block) gog cold init error: \(toString(err: error!))");
			});
		}

		
		return true
	}

	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}

	// MARK: - Split view

	func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
	    guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
	    guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController else { return false }
	    if topAsDetailController.detailItem == nil {
	        // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
	        return true
	    }
	    return false
	}

}

